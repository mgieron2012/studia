# Studia

Kilka z moich projektów wykonanych na studiach inżynierskich na kierunku Informatyka na wydziale EiTI Politechniki Warszawskiej.

Umieściłem tylko projekty i zadania realizowane indywidualnie.

## Techniki kompilacji

- [Interpreter własnego języka programowania](https://gitlab.com/mgieron2012/interpreter)

## Paradygmaty programowania

- [Gierka w Haskellu](https://gitlab.com/mgieron2012/haskell-game)

## Architektura komputerów

- [Odczytywanie kodu kreskowego w assemblerze](https://gitlab.com/mgieron2012/odczytywanie-kodu-kreskowego-assembler)

## Systemy operacyjne

- [Semafory i system plików](https://gitlab.com/mgieron2012/soi)

## Wstęp do sztucznej inteligencji

- [Problem plecakowy](https://gitlab.com/mgieron2012/problem-plecakowy)
- [Metoda najszybszego wzrostu](https://gitlab.com/mgieron2012/metoda-najszybszego-wzrostu)
- [Algorytm ewolucyjny](https://gitlab.com/mgieron2012/klasyczny-algorytm-ewolucyjny)
- [Algorytm min-max z przycinaniem alfa-beta](https://gitlab.com/mgieron2012/min-max-z-przycinaniem-alfa-beta)
- [Drzewo decyzyjne ID3](https://gitlab.com/mgieron2012/drzewo-decyzyjne-id3)

## Probabilistyka

- [Rozkład normalny](https://gitlab.com/mgieron2012/rozklad-normalny)
- [Prawo wielkich liczb](https://gitlab.com/mgieron2012/prawo-wielkich-liczb)
- [Monte Carlo](https://gitlab.com/mgieron2012/monte-carlo)

## Podstawy informatyki i programowania

- [Symulacja epidemii Python](https://gitlab.com/mgieron2012/symulacja-epidemii)
